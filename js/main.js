// Show pages

function showabout()
{
	page_load();
	div_hide('#content_div');
	$.get('about.php', function(data) { $('#content_div').html(data); div_fadein('#content_div'); page_loaded('about'); });
}

function showlogin()
{
	page_load();
	div_hide('#content_div');

	$.get('login.php', function(data)
	{
		$('#content_div').html(data); 
		div_fadein('#content_div');
		page_loaded();

		var user_email = $('#user_email_input').val();
		var user_password = $('#user_password_input').val();

		if(user_email != '' && user_password != '')
		{
			setTimeout(function() { $('#login_form').submit(); }, 250);
		}
		else
		{
			input_focus('#user_email_input');
		}
	});
}

function shownew_user()
{
	page_load();
	div_hide('#content_div');
	$.get('login.php?new_user', function(data) { $('#content_div').html(data); div_fadein('#content_div'); page_loaded(); input_focus('#user_name_input'); });
	
}

function showforgot_password()
{
	page_load();
	div_hide('#content_div');
	$.get('login.php?forgot_password', function(data) { $('#content_div').html(data); div_fadein('#content_div'); page_loaded(); });
	
}

function showadminreservation()
{
	page_load();
	div_hide('#content_div');
	$.get('ar.php', function(data) { $('#content_div').html(data); div_fadein('#content_div'); page_loaded(); });
}

function controloccupancy()
{
	page_load();
	div_hide('#content_div');
	$.get('oc.php', function(data) { $('#content_div').html(data); div_fadein('#content_div'); page_loaded(); });
}

function showoccupancy()
{
	page_load();
	div_hide('#content_div');
	$.get('loc.php', function(data) { $('#content_div').html(data); div_fadein('#content_div'); page_loaded(); });
}

function showreservations()
{
	page_load('reservation');
	div_hide('#content_div');

	$.get('reservation.php', function(data)
	{
		$('#content_div').html(data);
		div_fadein('#content_div');

		$.get('reservation.php?day', function(data)
		{
			$('#reservation_table_div').html(data).slideDown('slow', function() { setTimeout(function() { div_fadein('#reservation_table_div'); }, 250); });
			page_loaded();
		});
	});
}

function changelocation(e)
{
	var loc = e.getAttribute('value');

	notify('Changement de salle...', 200);
	setTimeout(function()
	{
		$.get('index.php?location=', { location: loc }, function(data)
		{
			window.location.replace('.');
		});
	}, 500);
	return false;
}

function showday(day)
{
	page_load('day');
	div_hide('#reservation_table_div');

	$.get('reservation.php?current='+day, function(data)
	{
		$('#content_div').html(data);
		div_fadein('#content_div');

		$.get('reservation.php?day', function(data)
		{
			$('#reservation_table_div').html(data).slideDown('slow', function() { setTimeout(function() { div_fadein('#reservation_table_div'); }, 250); });
			page_loaded('day');
		});
	});
}

function showcp()
{
	page_load();
	div_hide('#content_div');
	$.get('cp.php', function(data) { $('#content_div').html(data); div_fadein('#content_div'); page_loaded(); });
}

function showhelp()
{
	page_load();
	div_hide('#content_div');
	$.get('help.php', function(data) { $('#content_div').html(data); div_fadein('#content_div'); page_loaded(); });
}

// Page load

function page_load(page)
{
	// All
	setTimeout(function()
	{
		if($('#content_div').css('opacity') == 0)
		{
			notify('Chargement en cours...', 300);
		}
	}, 500);

	// Individual
	if(page == 'reservation')
	{
		setTimeout(function()
		{
			if($('#reservation_table_div').is(':hidden'))
			{
				notify('Chargement en cours...', 300);
			}
		}, 500);
	}	
	else if(page == 'day')
	{
		setTimeout(function()
		{
			if($('#reservation_table_div').css('opacity') == 0)
			{
				notify('Chargement en cours...', 300);
			}
		}, 500);
	}
}

function page_loaded(page)
{
	// All
	$.get('main.php?day_number', function(data)
	{
		//if(data != global_current_day)
		//{
		//	notify('Day have changed. Refreshing...', '300');
		//	setTimeout(function() { window.location.replace('.'); }, 2000);
		//}
	});

	setTimeout(function()
	{
		if($('#notification_inner_cell_div').is(':visible') && $('#notification_inner_cell_div').html() == 'Chargement en cours...')
		{
			notify();
		}
	}, 1000);

	read_reservation_details();
}

// Login

function login()
{
	var user_name = $('#user_name_input').val();
	var user_password = $('#user_password_input').val();

	$('#login_message_p').html('<img src="img/loading.gif" alt="Loading"> Connexion en cours...').slideDown('fast');

	var remember_me_checkbox = $('#remember_me_checkbox').prop('checked');

	if(remember_me_checkbox)
	{
		var user_remember = 1;
	}
	else
	{
		var user_remember = 0;
	}

	$.post('login.php?login', { user_name: user_name, user_password: user_password, user_remember: user_remember }, function(data)
	{
		if(data == 1)
		{
			input_focus();
			setTimeout(function() { window.location.replace('.'); }, 1000);
		}
		else
		{
			if(data == '')
			{
				$('#login_message_p').html('<span class="error_span">Nom ou mot de passe erron&eacute;s</span>');
				$('#user_email_input').val('');
				$('#user_password_input').val('');
				input_focus('#user_email_input');
			}
			else
			{
				$('#login_message_p').html(data);
			}
		}
	});
}

function logout()
{
	notify('D&eacute;connexion en cours...', 300);
	$.get('login.php?logout', function(data) { setTimeout(function() { window.location.replace('.'); }, 1000); });
}

function create_user()
{
	var user_name = $('#user_name_input').val();
	var user_email = $('#user_email_input').val();
	var user_password = $('#user_password_input').val();
	var user_password_confirm = $('#user_password_confirm_input').val();

	if($('#user_secret_code_input').length)
	{
		var user_secret_code =  $('#user_secret_code_input').val();
	}
	else
	{
		var user_secret_code = '';
	}

	if(user_password != user_password_confirm)
	{
		$('#new_user_message_p').html('<span class="error_span">Passwords do not match</span>').slideDown('fast');
		$('#user_password_input').val('');
		$('#user_password_confirm_input').val('');
		input_focus('#user_password_input');
	}
	else
	{
		$('#new_user_message_p').html('<img src="img/loading.gif" alt="Loading"> Creating user...').slideDown('fast');

		$.post('login.php?create_user', { user_name: user_name, user_email: user_email, user_password: user_password, user_secret_code: user_secret_code }, function(data)
		{
			if(data == 1)
			{
				input_focus();

				setTimeout(function()
				{
					$('#new_user_message_p').html('User created successfully! Logging in... <img src="img/loading.gif" alt="Loading">');
					setTimeout(function() { window.location.replace('#login'); }, 2000);
				}, 1000);
			}
			else
			{
				input_focus();
				$('#new_user_message_p').html(data);
			}
		});
	}
}

// Reservation

function toggle_reservation_time(id, month, day, year, time, place, from)
{
	var user_name = $(id).html();

	if(user_name == '')
	{
		$(id).html('Patience...'); 

		$.post('reservation.php?make_reservation', { month: month, day: day, year: year, time: time, place: place }, function(data) 
		{
			if(data == 1)
			{
				setTimeout(function() { read_reservation(id, month, day, year, time, place); }, 1000);
			}
			else
			{
				notify(data, 4);
				setTimeout(function() { read_reservation(id, month, day, year, time, place); }, 2000);			
			}
		});
	}
	else
	{
		if(typeof place == 'undefined' || place == '0')
		{
			$(id).html('Patience...'); 

			$.post('reservation.php?make_reservation', { month: month, day: day, year: year, time: time, place: place }, function(data) 
			{
				if(data == 1)
				{
					setTimeout(function() { showreservations(); }, 1000);
				}
				else
				{
					notify(data, 4);
					setTimeout(function() { showreservations(); }, 2000);			
				}
			});
		}
		else
		{
			if(offclick_event == 'mouseup' || from == 'details')
			{
				if(user_name == 'Patience...')
				{
					notify('Un seul clic suffit', 4);
				}
				else if(user_name == session_user_name || session_user_is_admin != '0')
				{
					if(user_name != session_user_name && session_user_is_admin != '0')
					{
						var delete_confirm = confirm('Ce n\'est pas votre réservation mais comme vous êtes administrateur, vous pouvez supprimer les réservations d\'autres utilisateurs. Voulez-vous réellement faire cela ?');
					}
					else
					{
						var delete_confirm = true;
					}

					if(delete_confirm)
					{
						$(id).html('Patience...');

						$.post('reservation.php?delete_reservation', { month: month, day: day, year: year, time: time, place: place }, function(data)
						{
							if(data == 1)
							{
								setTimeout(function() { read_reservation(id, month, day, year, time, place); }, 1000);
							}
							else
							{
								notify(data, 4);
								setTimeout(function() { read_reservation(id, month, day, year, time, place); }, 2000);
							}
						});
					}
				}
				else
				{
					notify('Vous ne pouvez pas supprimer les r&eacute;servations d\'autres utilisateurs.', 2);
				}

				if($('#reservation_details_div').is(':visible'))
				{
					read_reservation_details();
				}
			}
		}
	}
}

function read_reservation(id, month, day, year, time, place)
{
	$.post('reservation.php?read_reservation', { month: month, day: day, year: year, time: time, place: place }, function(data) { $(id).html(data); });
}

function read_reservation_details(id, month, day, year, time, place)
{
	if(typeof id != 'undefined' && $(id).html() != '' && $(id).html() != 'Patience...')
	{
		if($('#reservation_details_div').is(':hidden'))
		{
			var position = $(id).position();
			var top = position.top + 50;
			var left = position.left - 100;

			$('#reservation_details_div').html('Lecture...');
			$('#reservation_details_div').css('top', top+'px').css('left', left+'px');
			$('#reservation_details_div').fadeIn('fast');

			reservation_details_id = id;
			reservation_details_month = month;
			reservation_details_day = day;
			reservation_details_year = year;
			reservation_details_time = time;
			reservation_details_place = place;

			$.post('reservation.php?read_reservation_details', { month: month, day: day, year: year, time: time, place: place }, function(data)
			{
				setTimeout(function()
				{
					if(data == 0)
					{
						$('#reservation_details_div').html('Cette r&eacute;servation n\'existe plus. Patience...');
						
						setTimeout(function()
						{
							if($('#reservation_details_div').is(':visible') && $('#reservation_details_div').html() == 'Cette r&eacute;servation n\'existe plus. Patience...')
							{
								read_reservation(reservation_details_id, reservation_details_month, reservation_details_day, reservation_details_year, reservation_details_time, reservation_details_place);
								read_reservation_details();
							}
						}, 2000);
					}
					else
					{
						$('#reservation_details_div').html(data);

						if(offclick_event == 'touchend')
						{
							if($(reservation_details_id).html() == session_user_name || session_user_is_admin != '0')
							{
								var delete_link_html = '<a href="." onclick="toggle_reservation_time(reservation_details_id, reservation_details_month, reservation_details_day, reservation_details_year, reservation_details_time, \'details\'); return false">Delete</a> | ';
							}
							else
							{
								var delete_link_html = '';
							}

							$('#reservation_details_div').append('<br/><br/>'+delete_link_html+'<a href="." onclick="read_reservation_details(); return false">Close this</a>');
						}
					}
				}, 500);
			});
		}
	}
	else
	{
		$('div#reservation_details_div').fadeOut('fast');
	}
}

// Admin control panel
function set_reservation_user(id, time, user_id)
{
	$.post('ar.php?set_reservation', { time: time, user_id: user_id }, function(data)
	{
		if(data == 1)
		{
			setTimeout(function() { window.location.replace('.'); }, 500);
		}
		else
		{
			$('#reservation_div').html(data);
		}
	});
	
}

function list_users()
{
	$.get('cp.php?list_users', function(data) { $('#users_div').html(data); });
}

function reset_user_password()
{
	if(typeof $(".user_radio:checked").val() !='undefined')
	{
		var user_id = $(".user_radio:checked").val();

		$('#user_administration_message_p').html('<img src="img/loading.gif" alt="Loading">Réinitialisation du mot de passe...').slideDown('fast');

		$.post('cp.php?reset_user_password', { user_id: user_id }, function(data)
		{
			if(data == 0)
			{
				$('#user_administration_message_p').html('<span class="error_span">Vous pouvez changer votre mot de passe en bas de page</span>').slideDown('fast');
			}
			else
			{
				setTimeout(function() { $('#user_administration_message_p').html(data); }, 1000);
			}
		});
	}
	else
	{
		$('#user_administration_message_p').html('<span class="error_span">You must pick a user</span>').slideDown('fast');
	}
}

function change_user_permissions()
{
	if(typeof $(".user_radio:checked").val() !='undefined')
	{
		var user_id = $(".user_radio:checked").val();

		$('#user_administration_message_p').html('<img src="img/loading.gif" alt="Loading"> Changing permissions...').slideDown('fast');

		$.post('cp.php?change_user_permissions', { user_id: user_id }, function(data)
		{
			if(data == 1)
			{
				setTimeout(function()
				{
					list_users();
					$('#user_administration_message_p').html('Permissions changed successfully. The user must re-login to get the new permissions');
				}, 1000);
			}
			else
			{
				$('#user_administration_message_p').html(data);
			}
		});
	}
	else
	{
		$('#user_administration_message_p').html('<span class="error_span">Vous devez s&eacute;lectionner un utilisateur</span>').slideDown('fast');
	}
}

function delete_user_data(delete_data)
{
	if(typeof $(".user_radio:checked").val() !='undefined')
	{
		var delete_confirm = confirm('Are you sure?');

		if(delete_confirm)
		{
			var user_id = $(".user_radio:checked").val();

			$('#user_administration_message_p').html('<img src="img/loading.gif" alt="Loading"> Suppression en cours...').slideDown('fast');

			$.post('cp.php?delete_user_data', { user_id: user_id, delete_data: delete_data }, function(data)
			{
				if(data == 1)
				{
					setTimeout(function()
					{
						$('#user_administration_message_p').slideUp('fast', function()
						{
							if(delete_data == 'reservations')
							{
								list_users();
								get_usage();
							}
							else if(delete_data == 'user')
							{
								list_users();
							}
						});
					}, 1000);
				}
				else
				{
					$('#user_administration_message_p').html(data);
				}
			});
		}
	}
	else
	{
		$('#user_administration_message_p').html('<span class="error_span">Vous devez s&eacute;lectionner un utilisateur</span>').slideDown('fast');
	}
}

function delete_all(delete_data)
{
	if(delete_data == 'reservations')
	{
		var delete_confirm = confirm('Etes-vous certain de vouloir supprimer TOUTES les r&eacute;servations ? Sauvegardez la base de donn&eacute; auparavant !');
	}
	else if(delete_data == 'users')
	{
		var delete_confirm = confirm('Etes-vous certain de vouloir supprimer TOUS les utilisateurs ? Sauvegardez la base de donn&eacute; auparavant !');
	}
	else if(delete_data == 'everything')
	{
		var delete_confirm = confirm('Etes-vous certain de vouloir TOUT supprimer ? Sauvegardez la base de donn&eacute; auparavant !');
	}

	if(delete_confirm)
	{
		$('#database_administration_message_p').html('<img src="img/loading.gif" alt="Loading"> Deleting...').slideDown('fast');

		$.post('cp.php?delete_all', { delete_data: delete_data }, function(data)
		{
			if(data == 1)
			{
				setTimeout(function()
				{
					if(delete_data == 'everything')
					{
						window.location.replace('#logout');
					}
					else
					{
						list_users();
						$('#database_administration_message_p').slideUp('fast');
					}
				}, 1000);
			}
			else
			{
				$('#database_administration_message_p').html(data);
			}
		});
	}
}

// User control panel

function get_usage()
{
	$.get('cp.php?get_usage', function(data) { $('#usage_div').html(data); });
}

function get_reservation_reminders()
{
	$.get('cp.php?get_reservation_reminders', function(data) { $('#reservation_reminders_span').html(data); });
}

function add_one_reservation()
{
	$('#usage_message_p').html('<img src="img/loading.gif" alt="Loading">Enregistrement en cours...').slideDown('fast');

	$.post('reservation.php?make_reservation', { month: '0', day: '0', year: '0', time: '0', place: '0' }, function(data)
	{
		if(data == 1)
		{
			setTimeout(function()
			{
				if($('#users_div').length)
				{
					list_users();
				}

				get_usage();
				$('#usage_message_p').slideUp('fast');
			}, 1000);
		}
		else
		{
			$('#usage_message_p').html(data);
		}
	});
}

function toggle_reservation_reminder()
{
	$('#settings_message_p').html('<img src="img/loading.gif" alt="Loading">Enregistrement en cours...').slideDown('fast');

	$.post('cp.php?toggle_reservation_reminder', function(data)
	{
		if(data == 1)
		{
			setTimeout(function()
			{
				if($('#users_div').length)
				{
					list_users();		
				}

				get_reservation_reminders();
				$('#settings_message_p').slideUp('fast');
			}, 1000);
		}
		else
		{
			$('#settings_message_p').html(data);
		}
	});
}

function change_user_details()
{
	var user_email = $('#user_email_input').val();
	var user_password = $('#user_password_input').val();
	var user_password_confirm = $('#user_password_confirm_input').val();

	if(user_password != user_password_confirm)
	{
		$('#user_details_message_p').html('<span class="error_span">Les mots de passe ne correspondent pas</span>').slideDown('fast');
		$('#user_password_input').val('');
		$('#user_password_confirm_input').val('');
		input_focus('#user_password_input');
	}
	else
	{	
		$('#user_details_message_p').html('<img src="img/loading.gif" alt="Loading">Enregistrement et rafraichissement...').slideDown('fast');

		$.post('cp.php?change_user_details', { user_email: user_email, user_password: user_password }, function(data)
		{
			if(data == 1)
			{
				input_focus();
				setTimeout(function() { window.location.replace('.'); }, 1000);
			}
			else
			{
				input_focus();
				$('#user_details_message_p').html(data);
			}
		});
	}
}

// Ocupancy control panel

function set_occupancy_user(id, time, user_id, status)
{
	$.post('oc.php?set_occupancy', { time: time, user_id: user_id , status: status }, function(data)
	{
		$('#user_'+user_id).html(data);
	});
}

function add_occupancy_user(id, time, user_id)
{
	$.post('oc.php?add_occupancy', { time: time, user_id: user_id }, function(data)
	{
		$('#occupancies_list_div').html(data);
	});
}

// UI

function div_fadein(id)
{
	setTimeout(function()
	{
		if(global_css_animations == 1)
		{
			$(id).addClass('div_fadein');
		}
		else
		{
			$(id).animate({ opacity: 1 }, 250);
		}
	}, 1);
}

function div_hide(id)
{
	$(id).removeClass('div_fadein');
	$(id).css('opacity', '0');
}

function notify(text, time)
{
	if(typeof text != 'undefined')
	{
		if(typeof notify_timeout != 'undefined')
		{
			clearTimeout(notify_timeout);
		}

		$('#notification_inner_cell_div').css('opacity', '1');

		if($('#notification_div').is(':hidden'))
		{
			$('#notification_inner_cell_div').html(text);
			$('#notification_div').slideDown('fast');
		}
		else
		{
			$('#notification_inner_cell_div').animate({ opacity: 0 }, 250, function() { $('#notification_inner_cell_div').html(text); $('#notification_inner_cell_div').animate({ opacity: 1 }, 250); });
		}

		notify_timeout = setTimeout(function() { $('#notification_inner_cell_div').animate({ opacity: 0 }, 250, function() { $('#notification_div').slideUp('fast'); }); }, 1000 * time);
	}
	else
	{
		if($('#notification_div').is(':visible'))
		{
			$('#notification_inner_cell_div').animate({ opacity: 0 }, 250, function() { $('#notification_div').slideUp('fast'); });
		}
	}
}

function input_focus(id)
{
	if(offclick_event == 'touchend')
	{
		$('input').blur();
	}
	if(typeof id != 'undefined')
	{
		$(id).focus();
	}
}

// Document ready

$(document).ready( function()
{
	// Detect touch support
	if('ontouchstart' in document.documentElement)
	{
		onclick_event = 'touchstart';
		offclick_event = 'touchend';
	}
	else
	{
		onclick_event = 'mousedown';
		offclick_event = 'mouseup';
	}

	// Visual feedback on click
	$(document).on(onclick_event, 'input:submit, input:button, .reservation_time_div', function() { $(this).css('opacity', '0.5'); });
	$(document).on(offclick_event+ ' mouseout', 'input:submit, input:button, .reservation_time_div', function() { $(this).css('opacity', '1.0'); });

	// Buttons
	$(document).on('click', '#reservation_today_button', function() { showday('today'); });
	$(document).on('click', '#reset_user_password_button', function() { reset_user_password(); });
	$(document).on('click', '#change_user_permissions_button', function() { change_user_permissions(); });
	$(document).on('click', '#delete_user_reservations_button', function() { delete_user_data('reservations'); });
	$(document).on('click', '#delete_user_button', function() { delete_user_data('user'); });
	$(document).on('click', '#delete_all_reservations_button', function() { delete_all('reservations'); });
	$(document).on('click', '#delete_all_users_button', function() { delete_all('users'); });
	$(document).on('click', '#delete_everything_button', function() { delete_all('everything'); });
	$(document).on('click', '#occupancy_without_reservation_button', function()
	{
		var array = this.value.split(' ');
		$.post('oc.php?list_groups', { time: array[2] }, function(data) { $('#groups_div').html(data); });
	});

	// Checkboxes
	$(document).on('click', '#reservation_reminders_checkbox', function() { toggle_reservation_reminder(); });

	// Forms
	$(document).on('submit', '#login_form', function() { login(); return false; });
	$(document).on('submit', '#new_user_form', function() { create_user(); return false; });
	$(document).on('submit', '#user_details_form', function() { change_user_details(); return false; });

	// Links
	$(document).on('click mouseover', '#user_secret_code_a', function() { div_fadein('#user_secret_code_div'); return false; });
	$(document).on('click', '#previous_day_a', function() { showday('previous'); return false; });
	$(document).on('click', '#next_day_a', function() { showday('next'); return false; });

	// Divisions
	$(document).on('mouseout', '.reservation_time_cell_div', function() { read_reservation_details(); });

	$(document).on('click', '.reservation_time_cell_div', function()
	{
		var array = this.id.split(':');
		toggle_reservation_time(this, array[1], array[2], array[3], array[4], array[5], array[0]);
	});

	$(document).on('mousemove', '.reservation_time_cell_div', function()
	{
		var array = this.id.split(':');
		read_reservation_details(this, array[1], array[2], array[3], array[4], array[5]);
	});

	$(document).on('click', '.reservation_places_cell_div', function()
	{
		var array = this.id.split(':');
		toggle_reservation_time(this, array[1], array[2], array[3], array[4], '0', array[0]);
	});

	$(document).on('click', '.time_cell_div', function()
	{
		var array = this.id.split(':');
		$.post('ar.php?list_groups', { time: array[1] }, function(data) { $('#groups_div').html(data); });
	});

	$(document).on('click', '.occupancy_cell_div', function()
	{
		var array = this.id.split(':');
		$.post(array[0]+'.php?list_reservations', { time: array[1] }, function(data)
		{
			$('#reservation_list_div').html(data);
		});
		if (array[0] != 'loc')
		{
			$.post(array[0]+'.php?list_occupancies', { time: array[1] }, function(data)
			{
				$('#occupancies_list_div').html(data);
			});
			$('#add_user_button_div').html('<hr/><p><input type="button" class="small_button green_button" id="occupancy_without_reservation_button" value="Ajouter &eacute;l&egrave;ve '+array[1]+'"></p>');
		}
	});

	$(document).on('click', '.group_cell_div', function()
	{
		var array = this.id.split(':');
		$.post(array[0]+'.php?list_users_group', { time: array[1], group_name: array[2] }, function(data) { $('#users_div').html(data); });
	});

	$(document).on('click', '.user_cell_div', function()
	{
		var array = this.id.split(':');
		set_reservation_user(this, array[1], array[2]);
	});

	$(document).on('click', '.occupancy_user_cell_div', function()
	{
		var array = this.id.split(':');
		set_occupancy_user(this, array[1], array[2]);
	});

	$(document).on('click', '.occupancy_add_user_div', function()
	{
		var array = this.id.split(':');
		add_occupancy_user(this, array[1], array[2], 1);
	});

	$(document).on('click', '.set_user_occupancy', function()
	{
		var array = this.id.split(':');
		set_occupancy_user(this, array[1], array[2], 1);
	});

	$(document).on('click', '.unset_user_occupancy', function()
	{
		var array = this.id.split(':');
		set_occupancy_user(this, array[1], array[2], 0);
	});

	// Mouse pointer
	$(document).on('mouseover', 'input:button, input:submit, .reservation_time_div', function() { this.style.cursor = 'pointer'; });
});

// Hash change

function hash()
{
	var hash = window.location.hash.slice(1);

	if(hash == '')
	{
		if(typeof session_logged_in != 'undefined')
		{
			showreservations();
		}
		else
		{
			showlogin();
		}
	}
	else
	{
		if(hash == 'about')
		{
			showabout();
		}
		else if(hash == 'new_user')
		{
			shownew_user();
		}
		else if(hash == 'forgot_password')
		{
			showforgot_password();
		}
		else if(hash == 'help')
		{
			showhelp();
		}
		else if(hash == 'cp')
		{
			showcp();
		}
		else if(hash == 'ar')
		{
			showadminreservation();
		}
		else if(hash == 'oc')
		{
			controloccupancy();
		}
		else if(hash == 'loc')
		{
			showoccupancy();
		}
		else if(hash == 'logout')
		{
			logout();
		}
		else
		{
			window.location.replace('.');
		}
	}
}

// Window load

$(window).load(function()
{
	// Make sure cookies are enabled
	$.cookie(global_cookie_prefix+'_cookies_test', '1');
	var test_cookies_cookie = $.cookie(global_cookie_prefix+'_cookies_test');

	if(test_cookies_cookie == null)
	{
		window.location.replace('error.php?error_code=3');
	}
	else
	{
		$.cookie(global_cookie_prefix+'_cookies_test', null);

		hash();

		$(window).bind('hashchange', function ()
		{
			hash();
		});
	}
});

// Settings

$(document).ready( function()
{
	$.ajaxSetup({ cache: false });
});
