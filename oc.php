<?php 
include_once('main.php');
setlocale(LC_TIME, 'fr_FR.utf8');

if(check_login() != true) { exit; }

if($_SESSION['user_is_admin'] == '0') { exit; }

if(isset($_GET['list_reservations']))
{
	list($month, $day, $year) = explode(':', $_SESSION['current_day']);
	$today = mktime(0, 0, 0, $month, $day, $year);

	$time = mysql_real_escape_string($_POST['time']);
	echo list_reservations($month, $day, $year, $time);
}
elseif(isset($_GET['list_occupancies']))
{
	$time = mysql_real_escape_string($_POST['time']);
	echo get_occupancies_without_reservation($time);
}
elseif(isset($_GET['list_groups']))
{
	$time = mysql_real_escape_string($_POST['time']);
	echo list_groups($time, 'occupancy');
}
elseif(isset($_GET['list_users_group']))
{
	$time = mysql_real_escape_string($_POST['time']);
	$group_name = mysql_real_escape_string($_POST['group_name']);
	echo list_users_group($time, $group_name, 'occupancy');
}
elseif(isset($_GET['set_occupancy']))
{
	$time = mysql_real_escape_string($_POST['time']);
	$user_id = mysql_real_escape_string($_POST['user_id']);
	$status = mysql_real_escape_string($_POST['status']);
	echo set_occupancy_user($time, $user_id, $status);
}
elseif(isset($_GET['add_occupancy']))
{
	$time = mysql_real_escape_string($_POST['time']);
	$user_id = mysql_real_escape_string($_POST['user_id']);
	echo add_occupancy_user($time, $user_id);
}
else
{
	echo '<div class="box_div" id="cp_div"><div class="box_top_div"><a href="#">R&eacute;servations</a> &gt; Appel (' . get_current_location_name() . ')</div><div class="box_body_div">';
	
	$time_items = split(";", get_configuration('times'));

	list($month, $day, $year) = explode(':', $_SESSION['current_day']);
	$today = mktime(0, 0, 0, $month, $day, $year);
	echo '<h3>Choisissez le cr&eacute;neau horaire - ' . strftime("%A %e %B %Y", $today) . '</h3>';

	echo '<table><tr>';
	foreach($time_items as $time)
	{
		echo '<td><div class="occupancy_cell_div" id="oc:' . $time . '" onclick="void(0)"><input type="button" class="small_button green_button" value="' . $time . '"/></div></td>';
	}		
	echo '</tr></table>';

	echo '<div id="reservation_list_div">' . list_reservations() . '</div>';

	echo '<div id="occupancies_list_div">' . get_occupancies_without_reservation() . '</div>';
	
	echo '<div id="add_user_button_div">&nbsp;</div>';

	echo '<div id="groups_div">' . list_groups() . '</div>';

	echo '<div id="users_div">' . list_users_group() . '</div>';
		
	echo '<div id="occupancy_div">' . set_occupancy_user() . '</div>';
}
?>
