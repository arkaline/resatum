<?php
include_once('main.php');
setlocale(LC_TIME, 'fr_FR.utf8');
?>

<div id="header_inner_div"><div id="header_inner_left_div">

<?php

if(isset($_SESSION['logged_in']))
{
	echo list_locations();
	echo '<a href="#help">Aide</a>';
}

?>

</div><div id="header_inner_center_div">

<?php

if(isset($_SESSION['logged_in']))
{
	echo '<b>' . strftime("%A %e %B %Y", time()) . '</b>';
}

?>

</div><div id="header_inner_right_div">

<?php

if(isset($_SESSION['logged_in']))
{
	if($_SESSION['user_is_admin'] != '0')
	{
		echo '<a href="#ar">R&eacute;server</a> | ';
		echo '<a href="#oc">Faire l\'appel</a> | ';
		echo '<a href="#loc">Lister pr&eacute;sents</a> | ';
	}
	echo '<a href="#cp">Administration</a> | <a href="#logout">D&eacute;connexion</a>';
}
else
{
	echo 'Non connect&eacute;';
}

?>

</div></div>
