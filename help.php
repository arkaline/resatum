<?php

include_once('main.php');

if(check_login() != true) { exit; }

?>

<div class="box_div" id="help_div">
<div class="box_top_div"><a href="#">R&eacute;servations</a> &gt; Aide</div>
<div class="box_body_div">

<h3>R&eacute;servations</h3>

<ul>
<li><b>Comment faire une r&eacute;servation ?</b><br/>Cliquer sur le cr&eacute;neau horaire o&ugrave; vous voulez aller au CDI.</li>
<li><b>Comment supprimer une r&eacute;servation ?</b><br/>Cliquer sur la r&eacute;servation que vous voulez supprimer.</li>
<li><b>Quelles sont les restritions de r&eacute;servations ?</b><br/>Vous ne pouvez pas ajouter ou supprimer de r&eacute;servation dans le passé ou plus d'une semaine en avance.</li>
</ul>

<h3>Autres</h3>

<ul>
<li><b>Commeger mon m&egrave;l ou mon mot de passe ?</b><br/>Vous pouvez faire cela en passant par la page <a href="#cp">Administration</a>.</li>
</ul>

<?php

if($_SESSION['user_is_admin'] != '0')
{

?>

<h3>Aide pour les AED</h3>

<ul>
<li><b>Comment r&eacute;server pour des &eacute;l&egrave;ves ?</b><br/>Vous pouvez faire cela en passant par la page <a href="#ar">R&eacute;servation pour autrui</a>.</li>
<li><b>Comment voir les &eacute;l&egrave;ves pr&eacute;sents au CDI ?</b><br/>Tout simplement en passant par la page <a href="#loc">Pr&eacute;sences CDI</a>.</li>
<li><b>Comment r&eacute;server un cr&eacute;neau horaire complet au CDI ?</b><br/>Cliquer sur l'ent&ecirc;te de colonne du cr&eacute;neau souhait&eacute;.</li>
</ul>

<h3>Aide pour le professeur documentaliste</h3>

<ul>
<li><b>Comment r&eacute;server pour des &eacute;l&egrave;ves ?</b><br/>Vous pouvez faire cela en passant par la page <a href="#ar">R&eacute;servation pour autrui</a>.</li>
<li><b>Comment voir les &eacute;l&egrave;ves pr&eacute;sents au CDI ?</b><br/>Tout simplement en passant par la page <a href="#loc">Pr&eacute;sences CDI</a>.</li>
<li><b>Comment faire l'appel des &eacute;l&egrave;ves pr&eacute;sents au CDI ?</b><br/>En passant par la page <a href="#oc">Appel CDI</a>.</li>
<li><b>Comment r&eacute;server un cr&eacute;neau horaire complet au CDI ?</b><br/>Cliquer sur l'ent&ecirc;te de colonne du cr&eacute;neau souhait&eacute;.</li>
</ul>

<?php

}

?>

</div></div>
