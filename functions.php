<?php
setlocale(LC_TIME, 'fr_FR.utf8','fra');

// Configuration

function get_configuration($data)
{
	$query = mysql_query("SELECT * FROM " . global_mysql_configuration_table . " WHERE location_id=" . $_SESSION['location_id'] . " AND config_key='" . $data . "'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	$configuration = mysql_fetch_array($query);
	mysql_free_result($query);
	return($configuration['config_value']);
}

// Password

function random_password()
{
	$npassword = array(chr(rand(1, 9)+ord('0')));
	array_push($npassword, chr(rand(1, 9)+ord('0')));
	array_push($npassword, chr(rand(0, 25)+ord('a')));
	array_push($npassword, chr(rand(0, 25)+ord('a')));
	array_push($npassword, chr(rand(1, 9)+ord('0')));
	array_push($npassword, chr(rand(1, 9)+ord('0')));
	array_push($npassword, chr(rand(1, 9)+ord('0')));
	return join("", $npassword);
}

function encrypt_password($password)
{
	$password = crypt($password, '$1$' . global_salt);
	return($password);
}

function add_salt($password)
{
	$password = '$1$' . substr(global_salt, 0, -1) . '$' . $password;
	return($password);
}

function strip_salt($password)
{
	$password = str_replace('$1$' . substr(global_salt, 0, -1) . '$', '', $password);
	return($password);	
}

// String manipulation

function modify_email($email)
{
	$email = str_replace('@', '(at)', $email);
	$email = str_replace('.', '(dot)', $email);
	return($email);
}

// String validation

function validate_user_name($user_name)
{
	if(preg_match('/^[a-z æøåÆØÅ]{2,12}$/i', $user_name))
	{
		return(true);
	}
}

function validate_user_email($user_email)
{
	if(filter_var($user_email, FILTER_VALIDATE_EMAIL) && strlen($user_email) < 51)
	{
		return(true);
	}
}

function validate_user_password($user_password)
{
	if(strlen($user_password) > 3 && trim($user_password) != '')
	{
		return(true);
	}
}

// User validation

function user_name_exists($user_name)
{
	$query = mysql_query("SELECT * FROM " . global_mysql_users_table . " WHERE user_name='$user_name'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

	if(mysql_num_rows($query) > 0)
	{
		mysql_free_result($query);
		return(true);
	}
	mysql_free_result($query);
	return(false);
}

function user_email_exists($user_email)
{
	$query = mysql_query("SELECT * FROM " . global_mysql_users_table . " WHERE user_email='$user_email'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

	if(mysql_num_rows($query) > 0)
	{
		mysql_free_result($query);
		return(true);
	}
	mysql_free_result($query);
	return(false);
}

// Login

function get_login_data($data)
{
	if($data == 'user_name' && isset($_COOKIE[global_cookie_prefix . '_user_name']))
	{
		return($_COOKIE[global_cookie_prefix . '_user_name']);
	}
	elseif($data == 'user_password' && isset($_COOKIE[global_cookie_prefix . '_user_password']))
	{
		return($_COOKIE[global_cookie_prefix . '_user_password']);
	}
}

function login($user_name, $user_password, $user_remember)
{
	$user_password_encrypted = encrypt_password($user_password);
	$user_password = add_salt($user_password);
	
	$query = mysql_query("SELECT * FROM " . global_mysql_users_table . " WHERE user_name='$user_name' AND user_password='$user_password_encrypted' OR user_email='$user_email' AND user_password='$user_password'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

	if(mysql_num_rows($query) == 1)
	{
		$user = mysql_fetch_array($query);
		mysql_free_result($query);

		$_SESSION['user_id'] = $user['user_id'];
		$_SESSION['user_is_admin'] = $user['user_is_admin'];
		$_SESSION['user_email'] = $user['user_email'];
		$_SESSION['user_name'] = $user['user_name'];
		$_SESSION['user_reservation_reminder'] = $user['user_reservation_reminder'];
		$_SESSION['logged_in'] = '1';
		$_SESSION['location_id'] = 0;

		// Date
		$starting_day = time();
		$week_days = split(";", get_configuration('days'));
			
		while(in_array(date('N', $starting_day), $week_days) != true)
		{
			$starting_day = strtotime("+1 day", $starting_day);
		}
		$_SESSION['current_day'] = date('n', $starting_day) . ':' . date('j', $starting_day) . ':' . date('Y', $starting_day);

		if($user_remember == '1')
		{
			$user_password = strip_salt($user['user_password']);

			setcookie(global_cookie_prefix . '_user_name', $user['user_name'], time() + 3600 * 24 * intval(global_remember_login_days));
			setcookie(global_cookie_prefix . '_user_password', $user_password, time() + 3600 * 24 * intval(global_remember_login_days));
		}
		
		return(1);
	}
	mysql_free_result($query);
}

function check_login()
{
	if(isset($_SESSION['logged_in']))
	{
		$user_id = $_SESSION['user_id'];
		$query = mysql_query("SELECT * FROM " . global_mysql_users_table . " WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		if(mysql_num_rows($query) == 1)
		{
			mysql_free_result($query);
			return(true);
		}
		else
		{
			logout();
			echo '<script type="text/javascript">window.location.replace(\'.\');</script>';
		}
		mysql_free_result($query);
	}
	else
	{
		logout();
		echo '<script type="text/javascript">window.location.replace(\'.\');</script>';
	}
}

function logout()
{
	session_unset();
	setcookie(global_cookie_prefix . '_user_name', '', time() - 3600);
	setcookie(global_cookie_prefix . '_user_password', '', time() - 3600);
}

function create_user($user_name, $user_email, $user_password, $user_secret_code)
{
	if(validate_user_name($user_name) != true)
	{
		return('<span class="error_span">Name must be <u>letters only</u> and be <u>2 to 12 letters long</u>. If your name is longer, use a short version of your name</span>');
	}
	elseif(validate_user_email($user_email) != true)
	{
		return('<span class="error_span">L\'adresse mel doit &ecirc;tre valide et faire moins de 50 caract&egrave;res</span>');
	}
	elseif(validate_user_password($user_password) != true)
	{
		return('<span class="error_span">Le mot de passe doit faire au moins 4 caract&egrave;res</span>');
	}
	elseif(global_secret_code != '0' && $user_secret_code != global_secret_code)
	{
		return('<span class="error_span">Code secret erron&eacute;</span>');
	}
	elseif(user_name_exists($user_name) == true)
	{
		return('<span class="error_span">Le nom est d&eacute;j&agrave; utilis&eacute;.</span>');
	}
	elseif(user_email_exists($user_email) == true)
	{
		return('<span class="error_span">Cette adresse mel est d&eacute;j&agrave; enregistr&eacute;e.<a href="#forgot_password">Mot de passe oubli&eacute; ?</a></span>');
	}
	else
	{
		$query = mysql_query("SELECT * FROM " . global_mysql_users_table . "")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		if(mysql_num_rows($query) == 0)
		{
			$user_is_admin = '1';
		}
		else
		{
			$user_is_admin = '0';
		}
		mysql_free_result($query);

		$user_password = encrypt_password($user_password);

		mysql_query("INSERT INTO " . global_mysql_users_table . " (user_is_admin,user_email,user_password,user_name,user_reservation_reminder) VALUES ($user_is_admin,'$user_email','$user_password','$user_name','0')")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		$user_password = strip_salt($user_password);

		setcookie(global_cookie_prefix . '_user_email', $user_email, time() + 3600 * 24 * intval(global_remember_login_days));
		setcookie(global_cookie_prefix . '_user_password', $user_password, time() + 3600 * 24 * intval(global_remember_login_days));

		return(1);
	}
}

function list_admin_users()
{
	$query = mysql_query("SELECT * FROM " . global_mysql_users_table . " WHERE user_is_admin='2' OR user_is_admin='3' ORDER BY user_name")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

	if(mysql_num_rows($query) < 1)
	{
		mysql_free_result($query);
		return('<span class="error_span">Il n\'y a pas d\'administrateur</span>');
	}
	else
	{
		$return = '<table id="forgot_password_table"><tr><th>Nom</th><th>Mel</th></tr>';

		$i = 0;

		while($user = mysql_fetch_array($query))
		{
			$i++;

			$return .= '<tr><td>' . $user['user_name'] . '</td><td><span id="email_span_' . $i . '"></span></td></tr><script type="text/javascript">$(\'#email_span_' . $i . '\').html(\'<a href="mailto:\'+$.base64.decode(\'' . base64_encode($user['user_email']) . '\')+\'">\'+$.base64.decode(\'' . base64_encode($user['user_email']) . '\')+\'</a>\');</script>';
		}

		$return .= '</table>';

		mysql_free_result($query);
		return($return);
	}
}

// Reservations

function highlight_day($day)
{
	$day = str_ireplace(global_day_name, '<span id="today_span">' . global_day_name . '</span>', $day);
	return $day;
}


function read_reservation($month, $day, $year, $time, $place)
{
	$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_time='$time' AND reservation_place='$place' AND reservation_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	$reservation = mysql_fetch_array($query);
	mysql_free_result($query);
	return($reservation['reservation_user_name']);
}

function read_reservation_details($month, $day, $year, $time, $place)
{
	$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_time='$time' AND reservation_place='$place' AND reservation_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	$reservation = mysql_fetch_array($query);
	mysql_free_result($query);

	if(empty($reservation))
	{
		return(0);
		
	}
	else
	{
		return('<b>R&eacute;servation du :</b> ' . $reservation['reservation_made_time'] . '<br/><b>Utilisateur :</b> ' . $reservation['reservation_user_name']);
	}
}

function make_reservation($month, $day, $year, $time, $place='undefined')
{
	$user_id = $_SESSION['user_id'];
	$user_email = $_SESSION['user_email'];
	$user_name = $_SESSION['user_name'];
	$max_places = get_configuration('max_places');
	$glp = explode('h', $time);
	$hstart = 0;
	if (count($glp) > 0) {
	  $hstart = intval($glp[0]);
	}

	if($_SESSION['user_is_admin'] != '0' && $_SESSION['user_is_admin'] != '1' && ($place=='undefined' || $place == '' || $place == '0'))
	{
		$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_time='$time' AND reservation_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		if(mysql_num_rows($query) > 0)
		{
			mysql_free_result($query);
			return ('Il y a d&eacute;j&agrave; des r&eacute;servations dans ce cr&eacute;neau. Vous devez les supprimer avant de tout r&eacute;server.');
		}
		mysql_free_result($query);

		for ($i = 1; $i <= $max_places; $i++)
		{
			mysql_query("INSERT INTO " . global_mysql_reservations_table . " (reservation_made_time,reservation_year,reservation_month,reservation_day,reservation_time,reservation_place,reservation_user_id,reservation_user_email,reservation_user_name,reservation_location) VALUES (now(),'$year','$month','$day','$time','$i','$user_id','$user_email','------'," . $_SESSION['location_id'] . ")")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
		}
		return (1);
	}
	elseif($_SESSION['user_is_admin'] == '0' && mktime($hstart, 0, 0, $month, $day, $year) < time())
	{
		return('Vous ne pouvez pas r&eacute;server une heure pass&eacutee ou entam&eacute;e.');
	}
	elseif($_SESSION['user_is_admin'] == '0' && mktime($hstart, 0, 0, $month, $day, $year) > strtotime('+1 week'))
	{
		return('Vous pouvez faire des r&eacute;servations jusqu\'&agrave; une semaine en avance seulement.');
	}
	else
	{
		// controle de la reservation de cette place dans ce creneau
		$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_time='$time' AND reservation_place='$place' AND reservation_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		if(mysql_num_rows($query) < 1)
		{
			mysql_free_result($query);

			// controle du nombre de reservations dans ce creneau
			$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_time='$time' AND reservation_user_name='$user_name' AND reservation_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
			if(mysql_num_rows($query) < 1)
			{
				mysql_free_result($query);
				
				// controle du nombre de reservations dans la même journee (pas plus d'une)
				$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_user_name='$user_name' AND reservation_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
				if(mysql_num_rows($query) < 1)
				{
					mysql_free_result($query);
					mysql_query("INSERT INTO " . global_mysql_reservations_table . " (reservation_made_time,reservation_year,reservation_month,reservation_day,reservation_time,reservation_place,reservation_user_id,reservation_user_email,reservation_user_name,reservation_location) VALUES (now(),'$year','$month','$day','$time','$place','$user_id','$user_email','$user_name'," . $_SESSION['location_id'] . ")")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
					return(1);
				}
				else
				{
					mysql_free_result($query);
					return('Vous avez d&eacute;j&agrave; r&eacute;serv&eacute; une heure dans la journ&eacute;e.');
				}					
			}
			else
			{
				mysql_free_result($query);
				return('Vous avez d&eacute;j&agrave; r&eacute;serv&eacute; une heure dans ce cr&eacute;neau.');
			}
		}
		else
		{
			mysql_free_result($query);
			return('Un autre utilisateur vient de faire cette r&eacute;servation.');
		}
	}
}

function delete_reservation($month, $day, $year, $time, $place)
{
	$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_time='$time' AND reservation_place='$place' AND reservation_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	$user = mysql_fetch_array($query);
	mysql_free_result($query);

	if(mktime($hstart, 0, 0, $month, $day, $year) < time() && $_SESSION['user_is_admin'] != '2' && $_SESSION['user_is_admin'] != '3')
	{
		return('Vous ne pouvez pas supprimer de r&eacute;servation pour une heure pass&eacutee ou entam&eacute;e.');
	}
	if($user['reservation_user_id'] == $_SESSION['user_id'] || $_SESSION['user_is_admin'] != '0')
	{
		mysql_query("DELETE FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_time='$time' AND reservation_place='$place' AND reservation_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		return(1);
	}
	else
	{
		return('Vous ne pouvez pas supprimer les r&eacute;servations d\'un autre utilisateur');
	}
}

// Admin control panel

function list_users()
{
	$query = mysql_query("SELECT * FROM " . global_mysql_users_table . " ORDER BY user_is_admin DESC, user_name")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

	$users = '<table id="users_table"><tr><th>ID</th><th>Admin</th><th>Nom</th><th>Groupe</th><th>Mel</th><th>Rappels</th><th>Usage</th><th></th></tr>';

	while($user = mysql_fetch_array($query))
	{
		$users .= '<tr id="user_tr_' . $user['user_id'] . '"><td><label for="user_radio_' . $user['user_id'] . '">' . $user['user_id'] . '</label></td><td>' . $user['user_is_admin'] . '</td><td><label for="user_radio_' . $user['user_id'] . '">' . $user['user_name'] . '</label></td><td><label for="user_radio_' . $user['user_id'] . '">' . $user['user_group'] . '</label></td><td><label for="user_radio_' . $user['user_id'] . '">' . $user['user_email'] . '</label></td><td>' . $user['user_reservation_reminder'] . '</td><td>' . count_reservations($user['user_id']) . '</td><td><input type="radio" name="user_radio" class="user_radio" id="user_radio_' . $user['user_id'] . '" value="' . $user['user_id'] . '"></td></tr>';
	}

	$users .= '</table>';
	mysql_free_result($query);

	return($users);
}

function reset_user_password($user_id)
{
	$password = random_password();
	$password_encrypted = encrypt_password($password);

	mysql_query("UPDATE " . global_mysql_users_table . " SET user_password='$password_encrypted' WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

	if($user_id == $_SESSION['user_id'])
	{
		return(0);
	}
	else
	{
		return('Le mot de passe de l\'utilisateur dont l\'ID est ' . $user_id . ' est maintenant "' . $password . '". L\'utilisateur peut se connecter et changer son mot de passe.');
	}
}

function change_user_permissions($user_id)
{
	if($user_id == $_SESSION['user_id'])
	{
		return('<span class="error_span">Vous ne pouvez pas utiliser vos super pouvoirs pour les supprimer</span>');
	}
	else
	{
		mysql_query("UPDATE " . global_mysql_users_table . " SET user_is_admin = 1 - user_is_admin WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		return(1);
	}
}

function delete_user_data($user_id, $data)
{
	if($user_id == $_SESSION['user_id'] && $data != 'reservations')
	{
		return('<span class="error_span">Les comportements auto-destructeurs ne sont pas autoris&eacute;s</span>');
	}
	else
	{
		if($data == 'reservations')
		{
			mysql_query("DELETE FROM " . global_mysql_reservations_table . " WHERE reservation_user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
		}
		elseif($data == 'user')
		{
			mysql_query("DELETE FROM " . global_mysql_users_table . " WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
			mysql_query("DELETE FROM " . global_mysql_reservations_table . " WHERE reservation_user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
		}

		return(1);
	}
}

function delete_all($data)
{
	$user_id = $_SESSION['user_id'];

	if($data == 'reservations')
	{
		mysql_query("DELETE FROM " . global_mysql_reservations_table . " WHERE reservation_user_id!='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	}
	elseif($data == 'users')
	{
		mysql_query("DELETE FROM " . global_mysql_users_table . " WHERE user_id!='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
		mysql_query("DELETE FROM " . global_mysql_reservations_table . " WHERE reservation_user_id!='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	}
	elseif($data == 'everything')
	{
		mysql_query("DELETE FROM " . global_mysql_users_table . "")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
		mysql_query("DELETE FROM " . global_mysql_reservations_table . "")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	}

	return(1);
}

// User control panel

function get_usage()
{
	$usage = '<table id="usage_table"><tr><th>R&acute;servations</th></tr><tr><td>' . count_reservations($_SESSION['user_id']) . '</td></tr></table>';
	return($usage);
}

function get_user_group($user_id)
{
	$query = mysql_query("SELECT * FROM " . global_mysql_users_table . " WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	$user = mysql_fetch_array($query);
	mysql_free_result($query);
	return($user['user_group']);
}

function get_user_level($user_id)
{
	$query = mysql_query("SELECT * FROM " . global_mysql_users_table . " WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	$user = mysql_fetch_array($query);
	mysql_free_result($query);
	return(intval($user['user_is_admin']));
}

function count_reservations($user_id)
{
	$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_user_id='$user_id' AND reservation_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	$count = mysql_num_rows($query);
	mysql_free_result($query);
	return($count);
}

function get_reservation_reminders()
{
	$user_id = $_SESSION['user_id'];
	$query = mysql_query("SELECT * FROM " . global_mysql_users_table . " WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	$user = mysql_fetch_array($query);
	mysql_free_result($query);

	if($user['user_reservation_reminder'] == 1)
	{
		$return = '<input type="checkbox" id="reservation_reminders_checkbox" checked="checked">';
	}
	else
	{
		$return = '<input type="checkbox" id="reservation_reminders_checkbox">';
	}

	return($return);
}

function toggle_reservation_reminder()
{
	$user_id = $_SESSION['user_id'];
	mysql_query("UPDATE " . global_mysql_users_table . " SET user_reservation_reminder = 1 - user_reservation_reminder WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

	return(1);
}

function change_user_details($user_email, $user_password)
{
	$user_id = $_SESSION['user_id'];

	if(!empty($user_email) && validate_user_email($user_email) != true)
	{
		return('<span class="error_span">L\'adresse mel doit &ecirc;tre valide et faire moins de 50 caract&egrave;res</span>');
	}
	elseif(validate_user_password($user_password) != true && !empty($user_password))
	{
		return('<span class="error_span">Le mot de passe doit avoir au moins 4 caract&egrave;res</span>');
	}
	elseif(user_email_exists($user_email) == true && $user_email != $_SESSION['user_email'])
	{
		return('<span class="error_span">Cette adresse mel est d&acute;j&agrave; utilis&eacute;e</span>');
	}
	else
	{
		if(empty($user_password))
		{
			mysql_query("UPDATE " . global_mysql_users_table . " SET user_email='$user_email' WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
		}
		else
		{
			$user_password = encrypt_password($user_password);

			mysql_query("UPDATE " . global_mysql_users_table . " SET user_email='$user_email', user_password='$user_password' WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
		}

		mysql_query("UPDATE " . global_mysql_reservations_table . " SET reservation_user_email='$user_email' WHERE reservation_user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		$_SESSION['user_email'] = $user_email;

		$user_password = strip_salt($user_password);

		setcookie(global_cookie_prefix . '_user_email', $user_email, time() + 3600 * 24 * intval(global_remember_login_days));
		setcookie(global_cookie_prefix . '_user_password', $user_password, time() + 3600 * 24 * intval(global_remember_login_days));

		return(1);
	}
}

// Admin reservation

function list_groups($time="", $type="")
{
	if(isset($time) && strlen($time) > 0)
	{
		$query = mysql_query("SELECT * FROM " . global_mysql_groups_table . " ORDER BY group_name DESC")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		$func = 'ar';
		if($type == 'occupancy') { $func = 'oc'; }

		$users = '<h3>Choisissez la classe - ' . $time . '</h3><table><tr>';

		$i = 0;
		while($group = mysql_fetch_array($query))
		{
			$users .= '<td><div class="group_cell_div" id="' . $func . ':' . $time . ':' . $group['group_name'] . '" onclick="void(0)"><input type="button" class="small_button green_button" value="' . $group['group_name'] . '"/></div></td>';
			$i += 1;
			if($i > 9)
			{
				$i = 0;
				$users .= '</tr><tr>';
			}
		}
		
		$users .= '</tr></table>';
		mysql_free_result($query);

		return($users);
	}
}

function list_users_group($time="", $group_name="", $type="")
{
	if(isset($group_name) && strlen($group_name) > 0)
	{
		$query = mysql_query("SELECT * FROM " . global_mysql_users_table . " WHERE user_group = '". $group_name. "' ORDER BY user_name")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
		
		$classdiv = 'user_cell_div';
		$func = 'ar';
		if($type == 'occupancy')
		{
			$classdiv = 'occupancy_add_user_div';
			$func = 'oc';
		}

		$users = '<h3>Choisissez l\'utilisateur dans la classe ' . $group_name . '</h3><table><tr>';

		$i = 0;
		while($user = mysql_fetch_array($query))
		{
			$users .= '<td><div class="' . $classdiv . '" id="' . $func . ':' . $time . ':' . $user['user_id'] . '" onclick="void(0)"><input type="button" class="small_button green_button" value="' . $user['user_name'] . '"/></div></td>';
			$i += 1;
			if($i > 5)
			{
				$i = 0;
				$users .= '</tr><tr>';
			}
		}
		
		$users .= '</tr></table><hr/>';
		mysql_free_result($query);

		return($users);
	}
}

function count_reservations_time($month, $day, $year, $time)
{
	$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_time='$time' AND reservation_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
	$nb_row = mysql_num_rows($query);
	mysql_free_result($query);

	return $nb_row;
}

function next_unreserved_place($month, $day, $year, $time)
{
	$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_time='$time' AND reservation_location=" . $_SESSION['location_id'] . " ORDER BY reservation_place")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

	$free_place = 1;
	while($res = mysql_fetch_array($query))
	{
		$place = $res['reservation_place'];
		if($place == $free_place)
		{
			$free_place += 1;
		}
		else
		{
			mysql_free_result($query);
			return $free_place;
		}
	}
	mysql_free_result($query);
	return $free_place;
}

function set_reservation_user($time="", $user_id="")
{
	list($month, $day, $year) = explode(':', $_SESSION['current_day']);
	$today = mktime(0, 0, 0, $month, $day, $year);

	$nb_places = count_reservations_time($month, $day, $year, $time);
	$max_places = get_configuration('max_places');

	if(isset($user_id) && strlen($user_id) > 0)
	{
		if ($nb_places < $max_places)
		{
			$query = mysql_query("SELECT * FROM " . global_mysql_users_table . " WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

			if(mysql_num_rows($query) != 1)
			{
				mysql_free_result($query);
				return ("Utilisateur inconnu : $user_id");
			}
			$user = mysql_fetch_array($query);
			mysql_free_result($query);
			$user_name = $user['user_name'];
			$user_email = $user['user_email'];
			
			$place = next_unreserved_place($month, $day, $year, $time);

			// contole que l utilisateur n a pas deja reserve pour ce creneau
			$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_time='$time' AND reservation_user_name='$user_name' AND reservation_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

			if(mysql_num_rows($query) < 1)
			{
				mysql_free_result($query);
				
				mysql_query("INSERT INTO " . global_mysql_reservations_table . " (reservation_made_time,reservation_year,reservation_month,reservation_day,reservation_time,reservation_place,reservation_user_id,reservation_user_email,reservation_user_name,reservation_location) VALUES (now(),'$year','$month','$day','$time','$place','$user_id','$user_email','$user_name'," . $_SESSION['location_id'] . ")")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

				// compter le nombre de reservations deja faites dans la journee pour cet eleve
				$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_user_name='$user_name' AND reservation_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
				$nbreserv = mysql_num_rows($query);
				mysql_free_result($query);

				return('R&eacute;servation pour ' . $user_name . ' - ' . strftime("%A %e %B %Y", $today) . ' - ' . $time . ' - Nombre de r&eacute;servations dans la journ&eacute;e : ' . $nbreserv);
			}
			else
			{
				mysql_free_result($query);
				return("<span class=\"error_span\">$user_name a d&eacute;j&agrave; fait une r&eacute;servation pour ce cr&eacute;neau</span>");
			}

			return (1);
		}
		else
		{
			return ("<span class=\"error_span\">Le nombre maximum de r&eacute;servations est atteint.</span>");
		}
	}
}

// Occupancy control panel

function list_reservations($month=0, $day=0, $year=0, $time="")
{
	if(isset($time) && strlen($time) > 0)
	{
		list($month, $day, $year) = explode(':', $_SESSION['current_day']);
		$today = mktime(0, 0, 0, $month, $day, $year);

		$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_time='$time' AND reservation_location=" . $_SESSION['location_id'] . " ORDER BY reservation_user_name")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		$table = "<hr/><h3>Liste des &eacute;l&egrave;ves ayant r&eacute;serv&eacute; - " . strftime("%A %e %B %Y", $today) . " - $time</h3>";
		$table .='<p><table id="reservations_table"><tr><th>Nom</th><th>Classe</th><th></th><th></th></tr>';

		while($res = mysql_fetch_array($query))
		{
			$user_name = $res['reservation_user_name'];
			$user_id = $res['reservation_user_id'];
			
			if (get_user_level($user_id) == 0)
			{
				$supquery = mysql_query("SELECT * FROM " . global_mysql_occupancy_table . " WHERE occupancy_month='$month' AND occupancy_day='$day' AND occupancy_year='$year' AND occupancy_time='$time' AND occupancy_user_name='$user_name' AND occupancy_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

				if(mysql_num_rows($supquery) == 1)
				{
					$occ = mysql_fetch_array($supquery);
					$status = $occ['occupancy_status'];
					$user_name = '<span class="green_span">' . $res['reservation_user_name'] . '</span>';
					if($status == 0) { $user_name = '<span class="red_span">' . $res['reservation_user_name'] . '</span>'; }
				}
				mysql_free_result($supquery);

				$table .= '<tr><td><div id="user_' . $user_id . '">' . $user_name . '</div></td><td>' . get_user_group($user_id) . '</td>';
				$table .= '<td><div class="set_user_occupancy" id="div:' . $time . ':' . $user_id . '" onclick="void(0)"><input type="button" class="small_button green_button" value="Pr&eacute;sent"/></div></td>';
				$table .= '<td><div class="unset_user_occupancy" id="div:' . $time . ':' . $user_id . '" onclick="void(0)"><input type="button" class="small_button red_button" value="Absent"/></div></td>';
				$table .='</tr>';
			}
		}

		$table .= '</table>';
		mysql_free_result($query);

		$table .= '</p>';

		return($table);
	}
}

function list_occupancies($month=0, $day=0, $year=0, $time="")
{
	if(isset($time) && strlen($time) > 0)
	{
		list($month, $day, $year) = explode(':', $_SESSION['current_day']);
		$today = mktime(0, 0, 0, $month, $day, $year);

		$query = mysql_query("SELECT * FROM " . global_mysql_occupancy_table . " WHERE occupancy_month='$month' AND occupancy_day='$day' AND occupancy_year='$year' AND occupancy_time='$time' AND occupancy_location=" . $_SESSION['location_id']." ORDER BY occupancy_status DESC, occupancy_user_name")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		$table = "<hr/><h3>Liste des &eacute;l&egrave;ves pr&eacute;sents - " . strftime("%A %e %B %Y", $today) . " - $time</h3>";
		$table .= '<table id="reservations_table"><tr><th></th><th>Nom</th><th>Classe</th><th></th></tr>';
		$i = 1;
		while($res = mysql_fetch_array($query))
		{
			$table .= '<tr><th>' . $i . '</th>';
			$table .= '<td>' . $res['occupancy_user_name'] . '</td><td>' . $res['occupancy_user_group'] . '</td>';
			$abs = "";
			if($res['occupancy_status'] == 0)
			{
				$abs = 'abs.';
			}
			$table .= '<td>' . $abs . '</td></tr>';
			$i += 1;
		}

		$table .= '</table>';
		mysql_free_result($query);
		
		return($table);
	}
}

function get_occupancies_without_reservation($time="")
{
	if(isset($time) && strlen($time) > 0)
	{
		list($month, $day, $year) = explode(':', $_SESSION['current_day']);
		$today = mktime(0, 0, 0, $month, $day, $year);

		// construit la liste des reservations sur ce creneau
		$reserv = array();
		$query = mysql_query("SELECT * FROM " . global_mysql_reservations_table . " WHERE reservation_month='$month' AND reservation_day='$day' AND reservation_year='$year' AND reservation_time='$time' AND reservation_location=" . $_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
		while($res = mysql_fetch_array($query))
		{
			array_push($reserv, $res['reservation_user_name']);
		}
		mysql_free_result($query);

		// liste maintenant les eleves sans reservation
		$query = mysql_query("SELECT * FROM " . global_mysql_occupancy_table . " WHERE occupancy_month='$month' AND occupancy_day='$day' AND occupancy_year='$year' AND occupancy_time='$time' AND occupancy_location=" . $_SESSION['location_id']." ORDER BY occupancy_user_name")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		$table = "<hr/><h3>Liste des &eacute;l&egrave;ves sans r&eacute;servation - " . strftime("%A %e %B %Y", $today) . " - $time</h3>";
		$table .= '<table id="occupanciess_table"><tr><th></th><th>Nom</th><th>Classe</th><th></th></tr>';
		$i = 1;
		while($res = mysql_fetch_array($query))
		{ 
			if (in_array($res['occupancy_user_name'], $reserv) == false)
			{
				$table .= '<tr><th>' . $i . '</th>';
				$table .= '<td>' . $res['occupancy_user_name'] . '</td><td>' . $res['occupancy_user_group'] . '</td>';
				$table .= '<td>' . $abs . '</td></tr>';
				$i += 1;
			}
		}

		$table .= '</table>';
		mysql_free_result($query);

		return($table);
	}
}

function set_occupancy_user($time="", $user_id="", $status=1)
{
	if(isset($user_id) &&  strlen($user_id) > 0)
	{
		$query = mysql_query("SELECT * FROM " . global_mysql_users_table . " WHERE user_id='$user_id'")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		if(mysql_num_rows($query) != 1)
		{
			mysql_free_result($query);
			return ('No user $user_id');
		}
		$user = mysql_fetch_array($query);
		mysql_free_result($query);
		$user_group = $user['user_group'];
		$user_name = $user['user_name'];

		list($month, $day, $year) = explode(':', $_SESSION['current_day']);
		$today = mktime(0, 0, 0, $month, $day, $year);
		
		$query = mysql_query("SELECT * FROM " . global_mysql_occupancy_table . " WHERE occupancy_month='$month' AND occupancy_day='$day' AND occupancy_year='$year' AND occupancy_time='$time' AND occupancy_user_name='$user_name' AND occupancy_user_group='$user_group' AND occupancy_location=".$_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

		if(mysql_num_rows($query) < 1)
		{
			mysql_query("INSERT INTO " . global_mysql_occupancy_table . " (occupancy_made_time,occupancy_year,occupancy_month,occupancy_day,occupancy_time,occupancy_user_name,occupancy_user_group,occupancy_status,occupancy_location) VALUES (now(),'$year','$month','$day','$time','$user_name','$user_group',$status," . $_SESSION['location_id'] . ")")or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
		}
		else
		{
			mysql_query("UPDATE " . global_mysql_occupancy_table . " SET occupancy_status='$status' WHERE occupancy_month='$month' AND occupancy_day='$day' AND occupancy_year='$year' AND occupancy_time='$time' AND occupancy_user_name='$user_name' AND occupancy_user_group='$user_group' AND occupancy_location=" . $_SESSION['location_id'])or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');
		}
		mysql_free_result($query);
		if($status == 1) { return('<span class="green_span">' . $user_name.'</span>'); }
		return('<span class="red_span">' . $user_name.'</span>');
	}
	return ("");
}

function add_occupancy_user($time="", $user_id="")
{
	set_occupancy_user($time, $user_id, 1);

	return (get_occupancies_without_reservation($time));
}

// Locations administration
function list_locations()
{
	$query = mysql_query("SELECT * FROM " . global_mysql_locations_table)or die('<span class="error_span"><u>MySQL error:</u> ' . htmlspecialchars(mysql_error()) . '</span>');

	$locations = "";
	while($loc = mysql_fetch_array($query))
	{
		$locations .= '<a href="javascript:void(0);" value="' . $loc['location_id'] . '" onclick="changelocation(this)">R&eacute;servations ' . $loc['location_name'] . '</a> | ';
	}
		
	mysql_free_result($query);

	return($locations);
}

function get_current_location_name()
{
	if(isset($_SESSION['location_id']))
	{
		$sql = "SELECT * FROM " . global_mysql_locations_table . " WHERE location_id=" . $_SESSION['location_id'];
	}
	else
	{
		return('CDI & Autonomie');
	}
	$query = mysql_query($sql)or die('<span class="error_span"><u>MySQL error:</u> ' . $sql . htmlspecialchars(mysql_error()) . '</span>');

	if(mysql_num_rows($query) < 1)
	{
		mysql_free_result($query);
		return('<span class="error_span">Il n\'y a pas de salle avec cet identifiant</span>');
	}

	$loc = mysql_fetch_array($query);
	mysql_free_result($query);
	return($loc['location_name']);
}

?>
