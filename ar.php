<?php 
include_once('main.php');
setlocale(LC_TIME, 'fr_FR.utf8');

if(check_login() != true) { exit; }

if($_SESSION['user_is_admin'] != '0')
{
	if(isset($_GET['list_groups']))
	{
		$time = mysql_real_escape_string($_POST['time']);
		echo list_groups($time);
	}
	elseif(isset($_GET['list_users_group']))
	{
		$time = mysql_real_escape_string($_POST['time']);
		$group_name = mysql_real_escape_string($_POST['group_name']);
		echo list_users_group($time, $group_name);
	}
	elseif(isset($_GET['set_reservation']))
	{
		$time = mysql_real_escape_string($_POST['time']);
		$user_id = mysql_real_escape_string($_POST['user_id']);
		echo set_reservation_user($time, $user_id);
	}
	else
	{
		echo '<div class="box_div" id="cp_div"><div class="box_top_div"><a href="#">R&eacute;servations</a> &gt; R&eacute;server (' . get_current_location_name() . ')</div><div class="box_body_div">';

		$time_items = split(";", get_configuration('times'));
		$max_places = get_configuration('max_places');

		list($month, $day, $year) = explode(':', $_SESSION['current_day']);
		$today = mktime(0, 0, 0, $month, $day, $year);
		echo '<h3>Choisissez le cr&eacute;neau horaire - ' . strftime("%A %e %B %Y", $today) . '</h3>';

		echo '<table><tr>';
		foreach($time_items as $time)
		{
			$nb_places = count_reservations_time($month, $day, $year, $time);
			if($nb_places < $max_places)
			{
				echo '<td><div class="time_cell_div" id="div:' . $time . '" onclick="void(0)"><input type="button" class="small_button green_button" value="' . $time . '"/></div></td>';
			}
			else
			{
				echo '<td><div><input type="button" class="small_button" value="' . $time . '"/></div></td>';
			}
		}		
		echo '</tr></table>';

		echo '<div id="groups_div">' . list_groups() . '</div>';

		echo '<div id="users_div">' . list_users_group() . '</div>';
			
		echo '<div id="reservation_div">' . set_reservation_user() . '</div>';
	}
}
?>
