<?php

include_once('main.php');

if(isset($_GET['login']))
{
	$user_name = mysql_real_escape_string($_POST['user_name']);
	$user_password = mysql_real_escape_string($_POST['user_password']);
	$user_remember = $_POST['user_remember'];
	echo login($user_name, $user_password, $user_remember);
}
elseif(isset($_GET['logout']))
{
	logout();
}
elseif(isset($_GET['create_user']))
{
	$user_name = mysql_real_escape_string(trim($_POST['user_name']));
	$user_email = mysql_real_escape_string($_POST['user_email']);
	$user_password = mysql_real_escape_string($_POST['user_password']);
	$user_secret_code = $_POST['user_secret_code'];
	echo create_user($user_name, $user_email, $user_password, $user_secret_code);
}
elseif(isset($_GET['new_user']))
{

?>

	<div class="box_div" id="login_div"><div class="box_top_div"><a href="#">R&eacute;servations</a> &gt; Nouvel utilisateur</div><div class="box_body_div">
	<div id="new_user_div"><div>

	<form action="." id="new_user_form"><p>

	<label for="user_name_input">Nom :</label><br/>
	<input type="text" id="user_name_input"/><br/><br/>
	<label for="user_email_input">Mel :</label><br/>
	<input type="text" id="user_email_input" autocapitalize="off"/><br/><br/>
	<label for="user_password_input">Mot de passe :</label><br/>
	<input type="password" id="user_password_input"/><br/><br/>
	<label for="user_password_confirm_input">Confirmer le mot de passe :</label><br/>
	<input type="password" id="user_password_confirm_input"/><br/><br/>

<?php

	if(global_secret_code != '0')
	{
		echo '<label for="user_secret_code_input">Code secret : <sup><a href="." id="user_secret_code_a" tabindex="-1">Qu\'est ce que c\'est ?</a></sup></label><br/><input type="password" id="user_secret_code_input"><br/><br/>';
	}

?>

	<input type="submit" value="Cr&eacute;er utilisateur">

	</p></form>

	</div><div>
	
	<p class="green_p bold_p">Information :</p>
	<ul>
	<li>With just a click you can make your reservation</li>
	<li>Your usage is stored automatically</li>
	<li>Your password is encrypted and can't be read</li>
	</ul>

	<div id="user_secret_code_div">Le code secret est utilis&eacute; pour autoriser des administrateurs &agrave; cr&eacute;er des utilisateurs. Contacter le webmaster &agrave; l'adresse<span id="email_span"></span> pour obtenir le code secret.</div>

	<script type="text/javascript">$('#email_span').html('<a href="mailto:'+$.base64.decode('<?php echo base64_encode(global_webmaster_email); ?>')+'">'+$.base64.decode('<?php echo base64_encode(global_webmaster_email); ?>')+'</a>');</script>

	</div></div>

	<p id="new_user_message_p"></p>

	</div></div>

<?php

}
elseif(isset($_GET['forgot_password']))
{

?>

	<div class="box_div" id="login_div"><div class="box_top_div"><a href="#">R&eacute;servations</a> &gt; Mot de passe perdu</div><div class="box_body_div">

	<p>Contacter un administrateur pour indiquer que vous avez perdu votre mot de passe.</p>

	<?php echo list_admin_users(); ?>

	</div></div>

<?php

}
else
{

?>

	<div class="box_div" id="login_div"><div class="box_top_div">Connexion</div><div class="box_body_div">

	<form action="." id="login_form" autocomplete="off"><p>

	<label for="user_name_input">Nom :</label><br/><input type="text" id="user_name_input" value="<?php echo get_login_data('user_name'); ?>" autocapitalize="off"><br/><br/>
	<label for="user_password_input">Mot de passe :</label><br/><input type="password" id="user_password_input" value="<?php echo get_login_data('user_password'); ?>"><br/><br/>
	<input type="checkbox" id="remember_me_checkbox" checked="checked"/> <label for="remember_me_checkbox">Se souvenir de moi</label><br/><br/>
	
	<input type="submit" value="Connexion"/>
	</p></form>

	<p id="login_message_p"></p>
	<p><a href="#forgot_password">Mot de passe perdu</a></p>

	</div></div>

<?php

}

?>
