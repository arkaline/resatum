<?php 
include_once('main.php');
setlocale(LC_TIME, 'fr_FR.utf8');

if(check_login() != true) { exit; }

if($_SESSION['user_is_admin'] == '0') { exit; }

if(isset($_GET['list_reservations']))
{
	list($month, $day, $year) = explode(':', $_SESSION['current_day']);
	$today = mktime(0, 0, 0, $month, $day, $year);

	$time = mysql_real_escape_string($_POST['time']);
	echo list_occupancies($month, $day, $year, $time);
}
else
{
	echo '<div class="box_div" id="cp_div"><div class="box_top_div"><a href="#">R&eacute;servations</a> &gt; El&egrave;ves pr&eacute;sents (' . get_current_location_name() . ')</div><div class="box_body_div">';

	$time_items = split(";", get_configuration('times'));

	list($month, $day, $year) = explode(':', $_SESSION['current_day']);
	$today = mktime(0, 0, 0, $month, $day, $year);
	echo '<h3>Choisissez le cr&eacute;neau horaire - ' . strftime("%A %e %B %Y", $today) . '</h3>';

	echo '<table><tr>';
	foreach($time_items as $time)
	{
		echo '<td><div class="occupancy_cell_div" id="loc:' . $time . '" onclick="void(0)"><input type="button" class="small_button green_button" value="' . $time . '"/></div></td>';
	}		
	echo '</tr></table>';

	echo '<div id="reservation_list_div">' . list_occupancies() . '</div>';
}
?>
