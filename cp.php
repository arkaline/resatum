<?php 

include_once('main.php');

if(check_login() != true) { exit; }

if($_SESSION['user_is_admin'] != '0' && isset($_GET['list_users']))
{
	echo list_users();
}
elseif($_SESSION['user_is_admin'] != '0' && isset($_GET['reset_user_password']))
{
	$user_id = mysql_real_escape_string($_POST['user_id']);
	echo reset_user_password($user_id);
}
elseif($_SESSION['user_is_admin'] != '0' && isset($_GET['change_user_permissions']))
{
	$user_id = mysql_real_escape_string($_POST['user_id']);
	echo change_user_permissions($user_id);
}
elseif($_SESSION['user_is_admin'] != '0' && isset($_GET['delete_user_data']))
{
	$user_id = mysql_real_escape_string($_POST['user_id']);
	$data = $_POST['delete_data'];
	echo delete_user_data($user_id, $data);
}
elseif($_SESSION['user_is_admin'] != '0' && isset($_GET['delete_all']))
{
	$data = $_POST['delete_data'];
	echo delete_all($data);
}
elseif($_SESSION['user_is_admin'] != '0' && isset($_GET['save_system_configuration']))
{
	$price = mysql_real_escape_string($_POST['price']);
	echo save_system_configuration($price);
}
elseif(isset($_GET['get_usage']))
{
	echo get_usage();
}
elseif(isset($_GET['get_reservation_reminders']))
{
	echo get_reservation_reminders();
}
elseif(isset($_GET['toggle_reservation_reminder']))
{
	echo toggle_reservation_reminder();
}
elseif(isset($_GET['change_user_details']))
{
	$user_email = mysql_real_escape_string($_POST['user_email']);
	$user_password = mysql_real_escape_string($_POST['user_password']);
	echo change_user_details($user_email, $user_password);
}
else
{
	echo '<div class="box_div" id="cp_div"><div class="box_top_div"><a href="#">R&eacute;servations</a> &gt; Administration</div><div class="box_body_div">';

	if($_SESSION['user_is_admin'] == '3' || $_SESSION['user_is_admin'] == '2')
	{

?>

		<h3>Administration des utilisateurs</h3>

		<div id="users_div"><?php echo list_users(); ?></div>

		<p class="center_p"><input type="button" class="small_button green_button" id="reset_user_password_button" value="R&eacute;initialiser mot de passe"> <input type="button" class="small_button green_button" id="change_user_permissions_button" value="Modifier permissions"> <input type="button" class="small_button" id="delete_user_reservations_button" value="Supprimer r&eacute;servations"> <input type="button" class="small_button" id="delete_user_button" value="Supprimer utilisateur"></p>
		<p class="center_p" id="user_administration_message_p"></p>

		<hr/>

<?php
	}
	if($_SESSION['user_is_admin'] == '3')
	{
?>

		<h3>Administration de la base de donn&eacute;e</h3>

		<p class="smalltext_p">Ces actions n&eacute;cessitent une confirmation.</p>

		<p><input type="button" class="small_button" id="delete_all_reservations_button" value="Supprimer toutes les r&eacute;servations"> <input type="button" class="small_button" id="delete_all_users_button" value="Supprimer tous les utilisateurs"> <input type="button" class="small_button" id="delete_everything_button" value="Tout supprimer"></p>

		<p id="database_administration_message_p"></p>

		<hr class="green_hr thick_hr"/>

<?php

	}
	if(global_reservation_reminders == '1')
	{

?>

	<h3>Vos donn&eacute;es</h3>

	<p class="smalltext_p">Avant de modifier quelque chose, merci de v&eacute;rifier que tout est correct.</p>
	<p><span id="reservation_reminders_span"><?php echo get_reservation_reminders(); ?></span> <label for="reservation_reminders_checkbox">Send me reservation reminders by email</label></p>

	<p id="settings_message_p"></p>

	<hr>

<?php

	}

?>

	<h3>Vos donn&eacute;es</h3>

	<p class="smalltext_p">Si le mot de passe est laiss&eacute; vide, il ne sera pas modifi&eacute;.</p>

	<form action="." id="user_details_form" autocomplete="off"><p>

	<div id="user_details_div">
		<table>
		<tr>
			<td>
			<label for="user_name_input">Nom : <?php echo $_SESSION['user_name']; ?></label>
			</td>
			<td>
			<label for="user_email_input">Mel :</label><br/>
			<input type="text" id="user_email_input" autocapitalize="off" value="<?php echo $_SESSION['user_email']; ?>">
			</td>
		</tr>
		<tr>
			<td>
			<label for="user_password_input">Mot de passe :</label><br/>
			<input type="password" id="user_password_input">
			</td>
			<td>
			<label for="user_password_confirm_input">Confirmer mot de passe:</label><br/>
			<input type="password" id="user_password_confirm_input">
			</td>
		</tr>
		</table>
	</div>

	<p><input type="submit" class="small_button green_button" value="Modifier mes donn&eacute;es"></p>

	</p></form>

	<p id="user_details_message_p"></p>

	</div></div>

<?php

}

?>
