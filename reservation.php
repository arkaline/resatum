<?php

include_once('main.php');
setlocale(LC_TIME, 'fr_FR.utf8','fra');

if(check_login() != true) { exit; }

if(isset($_GET['make_reservation']))
{
	$month = mysql_real_escape_string($_POST['month']);
	$day = mysql_real_escape_string($_POST['day']);
	$year = mysql_real_escape_string($_POST['year']);
	$time = mysql_real_escape_string($_POST['time']);
	$place = mysql_real_escape_string($_POST['place']);
	echo make_reservation($month, $day, $year, $time, $place);
}
elseif(isset($_GET['delete_reservation']))
{
	$month = mysql_real_escape_string($_POST['month']);
	$day = mysql_real_escape_string($_POST['day']);
	$year = mysql_real_escape_string($_POST['year']);
	$time = mysql_real_escape_string($_POST['time']);
	$place = mysql_real_escape_string($_POST['place']);
	echo delete_reservation($month, $day, $year, $time, $place);
}
elseif(isset($_GET['read_reservation']))
{
	$month = mysql_real_escape_string($_POST['month']);
	$day = mysql_real_escape_string($_POST['day']);
	$year = mysql_real_escape_string($_POST['year']);
	$time = mysql_real_escape_string($_POST['time']);
	$place = mysql_real_escape_string($_POST['place']);
	echo read_reservation($month, $day, $year, $time, $place);
}
elseif(isset($_GET['read_reservation_details']))
{
	$month = mysql_real_escape_string($_POST['month']);
	$day = mysql_real_escape_string($_POST['day']);
	$year = mysql_real_escape_string($_POST['year']);
	$time = mysql_real_escape_string($_POST['time']);
	$place = mysql_real_escape_string($_POST['place']);
	echo read_reservation_details($month, $day, $year, $time, $place);
}
elseif(isset($_GET['day']))
{	
	list($month, $day, $year) = explode(':', $_SESSION['current_day']);

	$time_items = split(";", get_configuration('times'));
	$max_places = get_configuration('max_places');
	
	$nb_col = count($times);
	echo '<table id="reservation_table"><colgroup span="1" id="reservation_time_colgroup"></colgroup><colgroup span="' . $nb_col . '" id="reservation_day_colgroup"></colgroup>';

	$days_row = '<tr><td id="reservation_corner_td">&nbsp;</td>';
	foreach($time_items as $time)
	{
		if($_SESSION['user_is_admin'] == '0' or $_SESSION['user_is_admin'] == '1')
		{
			$days_row .= '<th class="reservation_day_th">' . $time . '</th>';
		}
		else
		{
			$days_row .= '<td><div class="reservation_time_div"><div id="div:' . $month . ':' . $day . ':' . $year . ':' . $time . '" class="reservation_places_cell_div" onclick="void(0)">' . $time . '</div></div></td>';
		}
	}
	$days_row .= '</tr>';

	echo $days_row;

	for ($place = 1; $place <= $max_places; $place += 1)
	{
		echo '<tr><th class="reservation_time_th">' . $place . '</th>';

		foreach($time_items as $time)
		{
			echo '<td><div class="reservation_time_div"><div class="reservation_time_cell_div" id="div:' . $month . ':' . $day . ':' . $year . ':' . $time . ':' . $place .'" onclick="void(0)">' . read_reservation($month, $day, $year, $time, $place) . '</div></div></td>';
		}

		echo '</tr>';
	}

	echo '</table>';
}
else
{
	list($month, $day, $year) = explode(':', $_SESSION['current_day']);
	$today = mktime(0, 0, 0, $month, $day, $year);
	$week_days = split(";", get_configuration('days'));
	
	if(isset($_GET['current']))
	{
		$param = $_GET['current'];
		if ($param == 'next')
		{
			$next_day = strtotime("+1 day", $today);
			while(in_array(date('N', $next_day), $week_days) != true)
			{
				$next_day = strtotime("+1 day", $next_day);
			}
			$month = date('n', $next_day);
			$day = date('j', $next_day);
			$year = date('Y', $next_day);
		}
		elseif ($param == 'previous')
		{
			$previous_day = strtotime("-1 day", $today);
			while(in_array(date('N', $previous_day), $week_days) != true)
			{
				$previous_day = strtotime("-1 day", $previous_day);
			}
			$month = date('n', $previous_day);
			$day = date('j', $previous_day);
			$year = date('Y', $previous_day);
		}
		$today = mktime(0, 0, 0, $month, $day, $year);
		$_SESSION['current_day'] = date('n', $today) . ':' . date('j', $today) . ':' . date('Y', $today);
	}

	echo '</div><div class="box_div" id="reservation_div"><div class="box_top_div" id="reservation_top_div"><div id="reservation_top_left_div"><a href="." id="previous_day_a">&lt; Jour pr&eacute;c&eacute;dent</a></div><div id="reservation_top_center_div">R&eacute;servations pour <span id="day_number_span">' . strftime("%A %e %B %Y", $today) .'</span></div><div id="reservation_top_right_div"><a href="." id="next_day_a">Jour suivant &gt;</a></div></div><div class="box_body_div"><div id="reservation_table_div"></div></div></div><div id="reservation_details_div">';
}

?>
